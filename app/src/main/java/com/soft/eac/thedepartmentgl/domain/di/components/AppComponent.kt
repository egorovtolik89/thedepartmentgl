package com.soft.eac.thedepartmentgl.domain.di.components

import com.soft.eac.thedepartmentgl.domain.di.modules.AppModule
import com.soft.eac.thedepartmentgl.domain.di.modules.NetModule
import com.soft.eac.thedepartmentgl.misc.dialogs.DialogsFragment
import com.soft.eac.thedepartmentgl.presentation.credentials.auth.AuthFragment
import com.soft.eac.thedepartmentgl.presentation.credentials.loading.LoadingFragment
import com.soft.eac.thedepartmentgl.presentation.credentials.registration.RegistrationFragment
import com.soft.eac.thedepartmentgl.presentation.main.TestActivity
import com.soft.eac.thedepartmentgl.service.GameService
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AppModule::class,
    NetModule::class
])
interface AppComponent {

    fun inject(target: RegistrationFragment)
    fun inject(target: AuthFragment)
    fun inject(target: LoadingFragment)
    fun inject(target: DialogsFragment)
    fun inject(target: GameService)
    fun inject(target: TestActivity)
}