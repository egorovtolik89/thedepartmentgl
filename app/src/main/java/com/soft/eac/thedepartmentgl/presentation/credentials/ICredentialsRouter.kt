package com.soft.eac.thedepartmentgl.presentation.credentials

interface ICredentialsRouter {

    fun showLoading()
    fun showRegistration()
    fun showAuth()
}