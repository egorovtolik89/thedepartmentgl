package com.soft.eac.thedepartmentgl.service

import android.app.Notification
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Binder
import android.os.Handler
import android.os.IBinder
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.soft.eac.thedepartmentgl.App
import com.soft.eac.thedepartmentgl.R
import com.soft.eac.thedepartmentgl.domain.repositories.UserRepository
import com.soft.eac.thedepartmentgl.game.NetworkPlayer
import com.soft.eac.thedepartmentgl.game.model.GameState
import java.util.concurrent.atomic.AtomicInteger
import javax.inject.Inject

class GameService : Service() {

    companion object {

        private const val ARG_TITLE = "ARG_TITLE"

        fun start(context: Context, title: String) {
            println("start => title: $title")
            val intent = Intent(context, GameService::class.java)
            intent.putExtra(ARG_TITLE, title)
            ContextCompat.startForegroundService(context, intent)
        }
    }

    class GameBinder(
        val service: GameService
    ) : Binder()

    private val handler = Handler()
    private val binder = GameBinder(this)
    var player: NetworkPlayer? = null
        private set

    var stateListener: ((GameState) -> Unit)? = null

    @Inject
    lateinit var userRepository: UserRepository

    override fun onBind(p0: Intent?): IBinder? = binder

    override fun onCreate() {
        super.onCreate()
        startForeground(110, getNotification())

        App.appComponent.inject(this)

        startSession()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        intent?.let {
            println("started => title: ${it.getStringExtra(ARG_TITLE)}; startId: $startId")
        }

        return START_NOT_STICKY
    }

    private fun startSession() {

        val tokenProvider: () -> String = {
            userRepository.getToken()?.access ?: throw IllegalStateException("Undefined token")
        }

        val onErrorAuthListener: () -> String = {
            val token = userRepository.getToken() ?: throw IllegalStateException("Token undefined")
            userRepository.refreshToken(token)?.access ?: throw IllegalStateException("Token undefined")
        }

        val renderCounter = AtomicInteger()
        val renderListener: (GameState) -> Unit = {
            println(it)
            handler.post { stateListener?.invoke(it) }
            if (renderCounter.getAndIncrement() == 0)
                player?.ready()
        }

        player = NetworkPlayer(
            "212.75.210.227", 3456,
            tokenProvider, onErrorAuthListener, renderListener
        ).apply {
            start()
        }
    }

    fun onRender() {
        player?.render()
    }

    private fun getNotification(): Notification {

        return NotificationCompat.Builder (this, "")
            .setSmallIcon(R.mipmap.ic_launcher_round)
            .setContentTitle("Service")
            .setContentText("Game server")
            .setSubText("sub text")
            .setCategory(NotificationCompat.CATEGORY_SERVICE)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .build()
    }
}