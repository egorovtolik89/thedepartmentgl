package com.soft.eac.thedepartmentgl.base;

/**
 * Created by anatoliy on 23.12.16.
 */

public interface IOnBackPressedListener {

    /**
     * @return true - прервать выполнение onBackPressed
     */
    boolean onBackPressed();
}
