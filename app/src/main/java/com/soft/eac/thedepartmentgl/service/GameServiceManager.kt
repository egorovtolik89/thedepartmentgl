package com.soft.eac.thedepartmentgl.service

import android.app.Activity
import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import com.soft.eac.thedepartmentgl.App
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.locks.ReentrantLock

class GameServiceManager {

    companion object {

        private val instance = GameServiceManager()

        fun connect(call: (GameService) -> Unit) {
            instance.connectToService(call)
        }

        fun release() = instance.release()
    }

    private var service: GameService? = null
    private var serviceConnection: ServiceConnection? = null
    private val isCallConnectToService = AtomicBoolean()
    private val lock = ReentrantLock()

    private constructor()

    private fun connectToService(call: (GameService) -> Unit) {

        service?.let {
            call(it)
            return
        }

        if (lock.tryLock()) {

            if (isCallConnectToService.get()) {
                try { service?.let(call) } catch (e: Exception) { e.printStackTrace() }
                lock.unlock()
                return
            }

            try {

                isCallConnectToService.set(true)
                val connector = object : ServiceConnection {

                    override fun onServiceConnected(p0: ComponentName?, binder: IBinder?) {
                        service = (binder as GameService.GameBinder).service
                    }

                    override fun onServiceDisconnected(p0: ComponentName?) {
                        isCallConnectToService.set(false)
                        serviceConnection = null
                        service = null
                    }
                }
                serviceConnection = connector

                val context = App.appContext
                val intent = Intent(context, GameService::class.java)
                context.bindService(intent, connector, Activity.BIND_AUTO_CREATE)

            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                lock.unlock()
            }

        } else {
            lock.lock()
            try { service?.let(call) } catch (e: Exception) { e.printStackTrace() }
            lock.unlock()
        }
    }

    private fun release() {
        serviceConnection?.let {
            App.appContext.unbindService(it)
            serviceConnection = null
            service = null
        }
    }
}