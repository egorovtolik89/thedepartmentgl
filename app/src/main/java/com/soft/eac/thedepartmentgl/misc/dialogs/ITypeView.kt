package com.soft.eac.thedepartmentgl.misc.dialogs

import com.soft.eac.thedepartmentgl.domain.repositories.models.DialogItem

interface ITypeView {
    fun bind(data: DialogItem)
}