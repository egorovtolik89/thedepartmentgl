package com.soft.eac.thedepartmentgl.domain.repositories.rest.service

import com.soft.eac.thedepartmentgl.domain.repositories.models.rest.UploadedFile
import com.soft.eac.thedepartmentgl.domain.repositories.models.rest.User
import io.reactivex.Observable
import okhttp3.MultipartBody
import retrofit2.http.*

interface IDialogRestApiService {


    /**
     * Регистрация нового профиля пользователя
     */
    @Multipart
    @POST("/upload/v1/avatar")
    fun uploadAvatar(@Part file: MultipartBody.Part): Observable<UploadedFile>


    /**
     * Получить список пользователей сервера
     */
    @GET("/user/v1/users")
    fun getUsers(): Observable<List<User>>
}