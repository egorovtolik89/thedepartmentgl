package com.soft.eac.thedepartmentgl.misc.dialogs

import com.arellomobile.mvp.MvpView
import com.soft.eac.thedepartmentgl.domain.repositories.models.DialogItem
import com.soft.eac.thedepartmentgl.game.model.GameState

interface IDialogsView : MvpView {

    fun bindDialogs(dialogs: List<DialogItem>)
    fun onRender(state: GameState)
}