package com.soft.eac.thedepartmentgl.presentation.game.ui

import android.graphics.Canvas

interface IElementUI {

    fun render(canvas: Canvas)
}