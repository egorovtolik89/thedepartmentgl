package com.soft.eac.thedepartmentgl.base;

/**
 * Created by anatoliy on 23.12.16.
 * <p>
 * Интерфейс для класса, который должен реализовать прослушку и вызов события
 */

public interface IOnBackPressedProvider {

    void addOnBackPressedListener(IOnBackPressedListener l);

    void removeOnBackPressedListener(IOnBackPressedListener l);
}
