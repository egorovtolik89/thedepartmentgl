package com.soft.eac.thedepartmentgl.service

import android.app.AlarmManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.IBinder
import android.os.SystemClock
import com.soft.eac.thedepartmentgl.App

class AlarmService : Service() {

    companion object {

        private const val ARG_BUNDLE = "ARG_BUNDLE"
        private const val REQUEST_CODE_SINGLE = 999
        private const val REQUEST_CODE_INTERVAL = 1000

        fun exec(bundle: Bundle, delay: Long = 0) {
            println("AlarmService.exec => bundle: $bundle; delay: $delay")

            val context = App.appContext
            val alarm = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager

            val intent = Intent(context, AlarmService::class.java).apply {
                putExtra(ARG_BUNDLE, bundle)
            }
            val pending = PendingIntent.getService(
                context, REQUEST_CODE_SINGLE, intent, 0)

            alarm.set(AlarmManager.RTC, System.currentTimeMillis() + delay, pending)
        }


        fun exec(bundle: Bundle, interval: Long, delay: Long = 0) {
            println("AlarmService.exec => bundle: $bundle; interval: $interval; delay: $delay")

            val context = App.appContext
            val alarm = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager

            val intent = Intent(context, AlarmService::class.java).apply {
                putExtra(ARG_BUNDLE, bundle)
            }
            val pending = PendingIntent.getService(
                context, REQUEST_CODE_INTERVAL, intent, PendingIntent.FLAG_UPDATE_CURRENT)

            alarm.setRepeating(AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + delay,
                interval,
                pending
            )
        }


        fun stop() {
            println("AlarmService.stop")

            val context = App.appContext
            val alarm = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager

            val intent = Intent(context, AlarmService::class.java)
            val pending = PendingIntent.getService(
                context, REQUEST_CODE_INTERVAL, intent, PendingIntent.FLAG_UPDATE_CURRENT)

            alarm.cancel(pending)
        }
    }

    override fun onBind(p0: Intent?): IBinder? = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        val bundle = intent?.getBundleExtra(ARG_BUNDLE) ?: return START_NOT_STICKY
        println("AlarmService => startId: $startId; bundle: $bundle")

        return START_NOT_STICKY
    }
}