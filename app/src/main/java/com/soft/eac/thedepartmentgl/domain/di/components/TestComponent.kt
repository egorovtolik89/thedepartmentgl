package com.soft.eac.thedepartmentgl.domain.di.components

import com.soft.eac.thedepartmentgl.domain.di.scope.CustomScope
import com.soft.eac.thedepartmentgl.presentation.credentials.auth.AuthFragment
import dagger.Component

//@CustomScope
//@Component(
//    dependencies = [AppComponent::class]
//)
interface TestComponent {

    fun inject(target: AuthFragment)
}