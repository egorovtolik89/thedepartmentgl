package com.soft.eac.thedepartmentgl.misc.dialogs

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.soft.eac.thedepartmentgl.domain.repositories.DialogsRepository
import com.soft.eac.thedepartmentgl.domain.repositories.UserRepository
import com.soft.eac.thedepartmentgl.presentation.game.ui.TakeUI
import com.soft.eac.thedepartmentgl.service.GameServiceManager
import javax.inject.Inject

@InjectViewState
class DialogsPresenter : MvpPresenter<IDialogsView> {

    private val dialogsRepository: DialogsRepository
    private val userRepository: UserRepository

    @Inject
    constructor(dialogsRepository: DialogsRepository, userRepository: UserRepository) {
        this.dialogsRepository = dialogsRepository
        this.userRepository = userRepository
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        dialogsRepository.loadDialogs {
            viewState.bindDialogs(it)
        }

//        GameServiceManager.connect {
//            it.stateListener = { viewState.onRender(it) }
//            it.onRender()
//        }
    }

    fun onCell(take: TakeUI) {
//        GameServiceManager.connect {
//            it.player?.cell(take.index)
//        }
    }
}