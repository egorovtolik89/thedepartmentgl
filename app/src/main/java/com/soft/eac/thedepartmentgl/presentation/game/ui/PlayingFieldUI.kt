package com.soft.eac.thedepartmentgl.presentation.game.ui

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import com.soft.eac.thedepartmentgl.game.Const
import com.soft.eac.thedepartmentgl.game.model.GameState
import kotlin.random.Random

class PlayingFieldUI : IElementUI {

    companion object {

        private val bgPaint = Paint().apply { color = Color.YELLOW }
        private val linePaint = Paint().apply {
            color = Color.BLACK
            strokeWidth = 3f
        }
    }

    private val takes = mutableListOf<TakeUI>()

    var x: Int = 0
    var y: Int = 0

    var width: Int = 0
    var height: Int = 0

    init {

        val random = Random(System.currentTimeMillis())
        for (index in 0..8)
            takes.add(TakeUI(index).apply {
                state = random.nextInt(3)
            })
    }

    override fun render(canvas: Canvas) {

        canvas.drawRect(Rect(0 + x, 0 + y, width + x, height + y), bgPaint)
        val padding = (width * 0.035).toInt()

        var row = 0
        var col = 0
        val itemWidth = width / 3
        val itemHeight = height / 3

        for (take in takes) {

            take.x = col * itemWidth + padding + x
            take.y = row * itemHeight + padding + y

            take.width = itemWidth - 2 * padding
            take.height = itemHeight - 2 * padding

            take.render(canvas)

            if (++col == 3) {
                col = 0
                if (++row == 3)
                    break
            }
        }

        val iw = itemWidth.toFloat()
        val ih = itemHeight.toFloat()
        val w = width.toFloat()
        val h = height.toFloat()

        // vertical
        canvas.drawLine(iw + x, 0f, iw + x, h, linePaint)
        canvas.drawLine(2 * iw + x, 0f, 2 * iw + x, h, linePaint)

        // horizontal
        canvas.drawLine(0f, ih + y, w + x, ih + y, linePaint)
        canvas.drawLine(0f, 2 * ih + y, w + x, 2 * ih + y, linePaint)
    }

    fun onClick(x: Float, y: Float): TakeUI? {
        return takes.firstOrNull { it.x < x && it.x + it.width >= x && it.y < y && it.y + it.height >= y }
    }

    fun setGameState(state: GameState) {

        val game = state.game.toTypedArray()
        for (i in 0 until 9)
            takes.get(i).state = when (game[i]) {
                Const.SELECT_TYPE_CROSS -> TakeUI.STATE_CROSS
                Const.SELECT_TYPE_ZERO -> TakeUI.STATE_ZERO
                else -> TakeUI.STATE_UNDEFINED
            }

        if (state.winner != null)
            println("WIN!")
    }
}