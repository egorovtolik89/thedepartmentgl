package com.soft.eac.thedepartmentgl.domain.repositories.models.rest

data class UploadedFile(
    val path: String
)