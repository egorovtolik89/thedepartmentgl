package com.soft.eac.thedepartmentgl.presentation.game.ui

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import kotlin.math.min

open class TakeUI(
    val index: Int
) : IElementUI {

    companion object {
        const val STATE_UNDEFINED = 0
        const val STATE_CROSS = 1
        const val STATE_ZERO = 2

        val paintRed = Paint().apply { color = Color.RED }
        val paintBlue = Paint().apply { color = Color.BLUE }
        val paintYellow = Paint().apply { color = Color.YELLOW }
    }

    var x: Int = 0
    var y: Int = 0

    var width: Int = 0
    var height: Int = 0

    var state: Int = STATE_UNDEFINED

    override fun render(canvas: Canvas) {
        when (state) {
            STATE_CROSS -> renderCross(canvas)
            STATE_ZERO -> renderZero(canvas)
        }
    }

    private fun renderCross(canvas: Canvas) {

        val x = x.toFloat()
        val y = y.toFloat()
        val w = width.toFloat()
        val h = height.toFloat()

//        canvas.drawLine(x, y, x + w, y + h, paintRed)
//        canvas.drawLine(x + w, y, x, y + h, paintRed)

        val hw = w * 0.5f
        val hh = h * 0.5f
        val cx = x + hw
        val cy = y + hh
        val r = min(hw, hh)

        val x1 = cx - r
        val y1 = cy - r
        val x2 = cx + r
        val y2 = cy + r

        paintRed.strokeWidth = r * 0.1f
        canvas.drawLine(x1, y1, x2, y2, paintRed)
        canvas.drawLine(x2, y1, x1, y2, paintRed)
    }

    private fun renderZero(canvas: Canvas) {

        val x = x.toFloat()
        val y = y.toFloat()
        val hw = width.toFloat() * 0.5f
        val hh = height.toFloat() * 0.5f
        val cx = x + hw
        val cy = y + hh
        val r = min(hw, hh)

        canvas.drawCircle(cx, cy, r, paintBlue)
        canvas.drawCircle(cx, cy, r * 0.9f, paintYellow)
    }
}