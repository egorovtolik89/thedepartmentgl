package com.soft.eac.thedepartmentgl.exceptions

class AuthException(message: String) : Exception(message)