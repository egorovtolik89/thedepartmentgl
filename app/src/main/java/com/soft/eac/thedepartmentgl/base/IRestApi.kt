package com.soft.eac.hardinscientific.base

/**
 * Created by anatoliy on 23.07.18.
 */
interface IRestApi {
    fun cancelAllRequests()
}