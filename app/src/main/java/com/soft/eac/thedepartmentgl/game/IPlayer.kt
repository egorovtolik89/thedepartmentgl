package com.soft.eac.thedepartmentgl.game

// Реализация интерфейса удаленного управления
interface IPlayer {

    fun start()
    fun ready()
    fun cell(value: Int)
    fun exit()
    fun render()
}