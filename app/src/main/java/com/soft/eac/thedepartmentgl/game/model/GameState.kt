package com.soft.eac.thedepartmentgl.game.model

data class GameState(
    val status: Int,
    val game: List<Int>,
    val players: List<GamePlayer>,
    val winner: GamePlayer?
)