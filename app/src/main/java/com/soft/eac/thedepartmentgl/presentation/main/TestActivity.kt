package com.soft.eac.thedepartmentgl.presentation.main

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.AudioAttributes
import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.HandlerThread
import android.provider.MediaStore
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.soft.eac.thedepartmentgl.App
import com.soft.eac.thedepartmentgl.R
import com.soft.eac.thedepartmentgl.base.*
import com.soft.eac.thedepartmentgl.domain.di.modules.NetModule
import com.soft.eac.thedepartmentgl.service.AlarmService
import com.soft.eac.thedepartmentgl.service.TestReceiver
import kotlinx.android.synthetic.main.activity_test.*
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import java.util.*
import javax.inject.Inject

class TestActivity : AActivityForResult(), ITestView {

    companion object {

        private const val REQUEST_ACCESS_READ_EXTERNAL_STORAGE = 111
        private const val REQUEST_CODE_GALLERY = 666

        fun show() {
            App.appContext.let {
                it.startActivity(Intent(it, TestActivity::class.java).apply {
                    flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                })
            }
        }
    }

    @Inject
    @InjectPresenter
    lateinit var presenter: TestPresenter

    @ProvidePresenter
    fun providePresenter() = presenter

    override fun inject() {
        App.appComponent.inject(this)
    }

    private val receiver = TestReceiver()

    private var cacheFile: File? = null
    private var imagePath: String? = null
    private var imageStream: InputStream? = null
    private var mediaPlayer: MediaPlayer? = null

    private val onActivityResultListener: IOnActivityResultListener by lazy {
        object : IOnActivityResultListener {
            override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?): Boolean {

                val provider: AActivityForResult = this@TestActivity

                return when (requestCode) {

                    REQUEST_CODE_GALLERY -> {
                        removeOnActivityResultListener(this)
                        if (resultCode == Activity.RESULT_OK) {

                            val selectedImage = intent?.data
                            imagePath = getRealPathFromURI(selectedImage, provider)
                            if (imagePath != null || selectedImage != null) {
                                cacheFile = prepareCache()
                                var success = imagePath != null
                                if (!success && selectedImage != null) {
                                    try {
                                        imageStream = provider.contentResolver.openInputStream(selectedImage)
                                        success = imageStream != null
                                    } catch (e: Exception) {
                                        e.printStackTrace()
                                    }
                                }

                                if (success) {
                                    addOnRequestPermissionsResultListener(onRequestPermissionsResultListener)
                                    runRequestPermissions(
                                        arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                                        REQUEST_ACCESS_READ_EXTERNAL_STORAGE)
                                }
                            }
                        }
                        true
                    }

                    else -> false
                }
            }
        }
    }

    private val onRequestPermissionsResultListener: IOnRequestPermissionsResultListener by lazy {
        object : IOnRequestPermissionsResultListener {
            override fun onRequestPermissionsResult(
                requestCode: Int,
                permissions: Array<out String>,
                grantResults: IntArray
            ): Boolean {

                return when (requestCode) {

                    REQUEST_ACCESS_READ_EXTERNAL_STORAGE -> {
                        removeOnRequestPermissionsResultListener(this)
                        if (checkPermissions(grantResults) == 1f) {
                            val cacheFile = cacheFile
                            if (cacheFile != null && (imagePath != null || imageStream != null)) {
                                onPrepareImage(cacheFile, imagePath, imageStream)
                            }
                        }

                        return true
                    }

                    else -> false
                }
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)

        val url = "https://noisefx.ru/noise_base/1/00567.mp3" // your URL here
        MediaPlayer().apply {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                setAudioAttributes(AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build())
            else
                setAudioStreamType(AudioManager.STREAM_MUSIC)

            setDataSource(url)
            setOnPreparedListener {

                if (this@TestActivity.isDestroyed) {
                    it.release()
                    return@setOnPreparedListener
                }

                btnSound.isEnabled = true
                mediaPlayer = it
            }

            prepareAsync()
            start()
        }

        btnLogout.setOnClickListener {
            presenter.logout()
        }

        btnSound.setOnClickListener {
            val player = mediaPlayer ?: return@setOnClickListener
            if (player.isPlaying)
                player.pause()
            else {
                player.seekTo(0)
                player.start()
            }
        }

        btnStart.setOnClickListener {
//            val title = UUID.randomUUID().toString()
//            GameService.start(it.context, title)
//            TestReceiver.send(it.context, title)
//            AlarmService.exec(Bundle().apply {
//                putString("UUID", title)
//            }, 2000)

            AlarmService.exec(Bundle().apply {
                putLong("TIME", System.currentTimeMillis())
            }, 2000, 1000)
        }

        btnStop.setOnClickListener {
            AlarmService.stop()
        }


        btnRefresh.setOnClickListener {
            presenter.refreshToken()
        }

        btnAdd.setOnClickListener {

            val title = "${UUID.randomUUID()} => ${llContainer.childCount}"
//            addView(TextView(it.context).apply {
//                text = title
//                setOnClickListener {
//                    removeView(it)
//                }
//            })

            val view = View.inflate(it.context, R.layout.view_test_row, null)
            view.findViewById<TextView>(R.id.tvTitle)?.text = title
            view.findViewById<TextView>(R.id.btnDelete)?.setOnClickListener {
                removeView(view)
            }
            addView(view)
        }

        btnClear.setOnClickListener {
            llContainer.removeAllViews()
        }

        btnOpenGallery.setOnClickListener {
            openGallary()
        }
    }

    override fun onStart() {
        super.onStart()

        val handler = Handler(HandlerThread("test").apply { start() }.looper)

        registerReceiver(receiver, IntentFilter().apply {
            addAction(TestReceiver.DEFAULT_ACTION)
        }, null, handler)
    }

    override fun onStop() {
        super.onStop()
        unregisterReceiver(receiver)
        mediaPlayer?.let {
            it.release()
            mediaPlayer = null
        }
    }

    private fun addView(view: View) {

        llContainer.addView(view, ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        ))
    }

    private fun removeView(view: View) {
        llContainer.removeView(view)
    }

    private fun openGallary() {
        addOnActivityResultListener(onActivityResultListener)
        val intent = Intent(Intent.ACTION_PICK).apply { type = "image/*" }
        runActivityForResult(intent, REQUEST_CODE_GALLERY)
    }

    private val projection = arrayOf(MediaStore.Images.Media.DATA)
    private fun getRealPathFromURI(
        contentURI: Uri?,
        activity: Activity
    ): String? {

        if (contentURI == null)
            return null

        activity.contentResolver.query(contentURI, projection, null, null, null)?.use {
            it.moveToFirst()
            val idx = it.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
            return if (idx == -1) null else it.getString(idx)
        }

        return null
    }

    private fun prepareCache(): File {
        return File(Utils.getDraftExternalDir(App.appContext), UUID.randomUUID().toString())
    }

    private fun onPrepareImage(cacheFile: File, imagePath: String?, imageStream: InputStream?) {

        if (imagePath == null && imageStream == null)
            return

        Thread {

            val copyResult = when {
                imagePath != null -> Utils.copy(File(imagePath), cacheFile)
                imageStream != null -> Utils.copy(imageStream, FileOutputStream(cacheFile))
                else -> return@Thread
            }

            val sourcePath = cacheFile.absolutePath

            if (copyResult && !sourcePath.isNullOrEmpty()) {
                Utils.compressImage(sourcePath)
                val bitmap = BitmapFactory.decodeFile(sourcePath)
                runOnUiThread { onSuccess(bitmap, cacheFile) }
            }

            else
                runOnUiThread { onError() }

        }.start()
    }

    private fun onSuccess(bitmap: Bitmap, file: File) {
        ivImage.setImageBitmap(bitmap)
        presenter.upload(file)
    }

//    private fun onSuccess(file: File) {
//        ivAvatar.setImageBitmap(Bitma)
//    }

    private fun onError() {

    }

    override fun lock() {
        btnOpenGallery.isEnabled = false
    }

    override fun unlock() {
        btnOpenGallery.isEnabled = true
    }

    override fun onSuccess(url: String) {
        toast("File uploaded: $url")

        val fullPath = "${NetModule.DOMAIN_MAIN_API}$url"
        toast("Load image: $fullPath")

        Glide
            .with(this)
            .load(fullPath)
            .placeholder(R.mipmap.ic_launcher_round)
            .apply(RequestOptions.circleCropTransform())
            .into(ivAvatar)
    }

    override fun onError(message: String) {
        toast(message)
    }
}