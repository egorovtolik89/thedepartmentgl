package com.soft.eac.thedepartmentgl.domain.repositories.models

data class DialogItem(
    val title: String,
    val isType1: Boolean
)