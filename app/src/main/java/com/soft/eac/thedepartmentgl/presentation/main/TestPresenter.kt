package com.soft.eac.thedepartmentgl.presentation.main

import android.os.Handler
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.soft.eac.thedepartmentgl.base.SubRX
import com.soft.eac.thedepartmentgl.domain.repositories.DialogsRepository
import com.soft.eac.thedepartmentgl.domain.repositories.UserRepository
import com.soft.eac.thedepartmentgl.presentation.credentials.CredentialsActivity
import java.io.File
import javax.inject.Inject

@InjectViewState
class TestPresenter : MvpPresenter<ITestView> {

    private var userRepository: UserRepository
    private var dialogsRepository: DialogsRepository
    private val handler = Handler()

    @Inject
    constructor(
        userRepository: UserRepository,
        dialogsRepository: DialogsRepository
    ) {
        this.userRepository = userRepository
        this.dialogsRepository = dialogsRepository

        dialogsRepository.getUsers(SubRX { users, e ->
            users?.let { println("users: $users") }
            e?.printStackTrace()
        })
    }

    fun upload(file: File) {
        println("Upload file: $file")

        viewState.lock()
        dialogsRepository.uploadAvatar(SubRX { path, e ->

            viewState.unlock()
            e?.let {
                viewState.onError(it.localizedMessage)
                it.printStackTrace()
                return@SubRX
            }

            path?.let {
                println("Link: $it")
                viewState.onSuccess(it.path)
            }

        }, file)
    }

    fun logout() {
        userRepository.logout()
        CredentialsActivity.show()
    }

    fun refreshToken() {
        val token = userRepository.getToken() ?: return
        Thread {
            val newToken = userRepository.refreshToken(token) { it < 10 }
            if (newToken != null)
                handler.post { viewState.onError("Токен обновлен") }
            else
                handler.post { viewState.onError("Ошибка при обновлении токена") }
        }.start()
    }
}