package com.soft.eac.thedepartmentgl.presentation.credentials.auth

import com.soft.eac.thedepartmentgl.base.IBaseView

interface IAuthView : IBaseView {

    fun onSuccess()
}