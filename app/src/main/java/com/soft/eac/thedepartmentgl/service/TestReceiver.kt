package com.soft.eac.thedepartmentgl.service

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class TestReceiver : BroadcastReceiver() {

    companion object {

        const val DEFAULT_ACTION = "com.soft.eac.thedepartmentgl.service.TestReceiver.DEFAULT"
        private const val ARG_MESSAGE = "ARG_MESSAGE"

        fun send(context: Context, message: String) {
            println("send: $message")
            context.sendBroadcast(Intent().apply {
                action = DEFAULT_ACTION
                putExtra(ARG_MESSAGE, message)
            })
        }
    }

    override fun onReceive(p0: Context?, intent: Intent?) {
        intent ?: return
        val action = intent.action ?: return

        when (action) {
            DEFAULT_ACTION -> onDefault(intent)
        }
    }

    private fun onDefault(intent: Intent) {
        val name = Thread.currentThread().name
        println("onDefault[$name]: ${intent.getStringExtra(ARG_MESSAGE)}")
    }
}