package com.soft.eac.thedepartmentgl.domain.di.modules

import com.soft.eac.thedepartmentgl.domain.repositories.local.UserStorage
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideUserStorage() = UserStorage()
}