package com.soft.eac.thedepartmentgl.game

import com.google.gson.GsonBuilder
import com.soft.eac.thedepartmentgl.game.model.GameState
import eac.network.*
import java.util.concurrent.atomic.AtomicBoolean

class NetworkPlayer(

    val address: String,
    val port: Int,
    val tokenProvider: () -> String,
    val onErrorAuthListener: () -> String,
    val renderListener: (GameState) -> Unit

) : IPlayer {

    companion object {

        const val CMD_NEED_AUTHORIZATION = "AUTHORIZATION"
        const val CMD_RESULT_SUCCESS = "SUCCESS"
        const val CMD_DIVIDER = ":"
        const val CMD_SELECT_GAME = "GAME${CMD_DIVIDER}tic-tac-toe"
        const val CMD_RENDER = "RENDER$CMD_DIVIDER"

        const val CMD_READY = "READY"
        const val CMD_CELL = "CELL"
        const val CMD_EXIT = "EXIT"
        const val CMD_STATE = "STATE"
    }

    private var connection: Connection? = null
    private var sender = PackageSender()
    private var receiver = PackageReceiver()
    private var lastState: GameState? = null

    private val isAuth = AtomicBoolean()
    private val gson = GsonBuilder().create()

    override fun start() {

        val connection = connection
        if (connection != null)
            return

        val tcp = Tcp(address, port)
            .setOnDisconnected<Tcp> { disconnect() }

        this.connection = tcp

        tcp.start()
        sender.register(tcp)
        receiver.register(tcp) { _, bytes -> onMessage(String(bytes)) }
    }

    override fun ready() {
        send(CMD_READY)
    }

    override fun cell(value: Int) {
        send(CMD_CELL + CMD_DIVIDER + "$value")
    }

    override fun exit() {
        send(CMD_EXIT) { disconnect() }
    }

    override fun render() {
        val state = lastState
        if (state != null && state.status == 2)
            send(CMD_STATE)
    }

    private fun onMessage(message: String) {
        println("onMessage: $message")

        if (!isAuth.get()) {
            onAuth(message)
            return
        }

        if (message.startsWith(CMD_RENDER)) {
            val json = message.substring(CMD_RENDER.length).trim()
            val state = gson.fromJson(json, GameState::class.java)
            renderListener(state)
            if (state.winner != null)
                exit()
            return
        }
    }

    private fun onAuth(message: String) {

        when (message) {
            CMD_NEED_AUTHORIZATION -> send(tokenProvider())
            CMD_RESULT_SUCCESS -> onSuccessAuth()
            else -> onErrorAuth()
        }
    }

    private fun onSuccessAuth() {

        isAuth.set(true)
        send(CMD_SELECT_GAME)
    }

    private fun onErrorAuth() {
        val token = onErrorAuthListener()
        send(token)
    }

    private fun send(message: String, call: ((Boolean) -> Unit)? = null) {
        println("send: $message")
        sender.send(message, object : Connection.Call() {
            override fun onSuccess(data: Connection.Data) { call?.invoke(true) }
            override fun onError(data: Connection.Data) { call?.invoke(false) }
        })
    }

    private fun disconnect() {

        connection?.let {
            connection = null
            sender.unregister()
            receiver.unregister()
            it.shutdown()
        }
    }
}