package com.soft.eac.thedepartmentgl.presentation.credentials.loading

import android.os.Handler
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.soft.eac.thedepartmentgl.domain.repositories.UserRepository
import com.soft.eac.thedepartmentgl.presentation.main.MainActivity
import javax.inject.Inject

@InjectViewState
class LoadingPresenter : MvpPresenter<ILoadingView> {

    private val userRepository: UserRepository

    @Inject
    constructor(userRepository: UserRepository) {
        this.userRepository = userRepository
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        loadStaticResources()
    }

    fun loadStaticResources() {
        Handler().postDelayed({

            val token = userRepository.getToken()
            if (token != null) {
                MainActivity.show()
                return@postDelayed
            }

            viewState.showAuth()

        }, 500)
    }
}