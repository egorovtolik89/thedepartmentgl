package com.soft.eac.thedepartmentgl.domain.repositories

import com.soft.eac.thedepartmentgl.base.SubRX
import com.soft.eac.thedepartmentgl.base.standardSubscribeIO
import com.soft.eac.thedepartmentgl.domain.repositories.models.DialogItem
import com.soft.eac.thedepartmentgl.domain.repositories.models.rest.UploadedFile
import com.soft.eac.thedepartmentgl.domain.repositories.models.rest.User
import com.soft.eac.thedepartmentgl.domain.repositories.rest.api.DialogRestApi
import java.io.File
import java.util.*
import javax.inject.Inject

class DialogsRepository {

    private val rest: DialogRestApi

    @Inject
    constructor(rest: DialogRestApi) {
        this.rest = rest
    }

    fun loadDialogs(call: (List<DialogItem>) -> Unit) {

        val result = mutableListOf<DialogItem>()
        val random = Random(System.currentTimeMillis())
        val count = random.nextInt(900) + 100
        for (index in 0 until count)
            result.add(DialogItem("Title: $index", random.nextBoolean()))

        call(result)
    }

    fun uploadAvatar(observer: SubRX<UploadedFile>, file: File) {

        rest.uploadAvatar(file)
            .standardSubscribeIO(observer)
    }

    fun getUsers(observer: SubRX<List<User>>) {

        rest.getUsers()
            .standardSubscribeIO(observer)
    }
}